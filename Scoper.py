#!/usr/bin/env python3

from bs4 import BeautifulSoup
import json
import os
import urllib.parse


class Scoper:
    def __init__(self):
        self.files = None
        self.contents = ""
        self.soup = None
        self.in_scope = []
        self.out_scope = []
        self.scope = None

    def build_soup(self, file):
        print(f"Reading {file}...")
        with open(file, "r") as f:
            self.contents = f.read()

        self.soup = BeautifulSoup(self.contents, "html.parser")

    def extract_scopes(self):
        in_links = self.soup.find("div", id="webapps-pane-in-scope").findAll("a", class_="external-link")
        self.in_scope = self.build_scope_objects([link['href'] for link in in_links])

        out_links = self.soup.find("div", id="webapps-pane-out-scope") \
            .findAll("span", class_="target-show-scope-url-list-inactive-link")
        self.out_scope = self.build_scope_objects([link.text for link in out_links])

    @staticmethod
    def build_scope_objects(urls):
        scope_objects = []
        for url in urls:
            scope_objects.append({
                "enabled": "true",
                "protocol": "any",
                "host": urllib.parse.urlsplit(url).netloc
            })
        return scope_objects

    def build_scope(self):
        self.scope = {
            "target": {
                "scope": {
                    "exclude": self.out_scope,
                    "include": self.in_scope
                }
            }
        }

    def add_default_configs(self):
        if os.path.isdir("./config"):
            for file in os.listdir("./config"):
                try:
                    if file.endswith(".json"):
                        with open(os.path.join("./config", file), "r") as f:
                            self.scope.update(json.load(f))
                except json.decoder.JSONDecodeError:
                    print(f"{file} doesn't appear to be valid json... Skipping!")

    def write_file(self, file):
        if not file.endswith(".json"):
            file += ".json"
        print(f"Writing {file}...")
        with open(file, "w+") as f:
            f.write(json.dumps(self.scope))


if __name__ == "__main__":
    for item in os.listdir("./in"):
        if os.path.isfile(os.path.join("./in", item)) and not os.path.isfile(
                os.path.join("./out", item)) and not os.path.isfile(os.path.join("./out", item + ".json")):
            scoper = Scoper()
            scoper.build_soup(os.path.join("./in", item))
            scoper.extract_scopes()
            scoper.build_scope()
            scoper.add_default_configs()
            scoper.write_file(os.path.join("./out", item))
