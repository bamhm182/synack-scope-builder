# Synack Scope Builder

This project sets out to fix a problem I have with the Synack bug bounty platform. I would like to export all in-scope and out-of-scope URLs into one file that I can easily load into Burp Suite's Target => Scope window, but I don't see a way to be able to do that from within Synack.

## Usage

1. Clone this project
1. Visit the Synack Target you wish to build a scope file for. I'll use a made up target called `CrazyHusky` for reference
1. In the `Scope` tab, right-click directly above `Target Scope` and click `Inspect`. Ensure the `.panel-body` div is selected and copy it
1. Create a file in the `./in/` folder. I would make `./in/CrazyHuskey`. Paste the copied HTML into this file
1. Run `python3 ./Scoper.py`
1. Your scope json file will be located at `./out/CrazyHusky`

#### Add Extra Burp Suite Configurations

Most Burp Suite pages have the ability to export their current configuration into a json file. If you put these jsons files into the `./config/` directory, they should automatically be added to the file that gets made in `./out/`. That way, all you have to do is select that one file when loading Burp Suite.

## Requirements

* Python 3
* BeautifulSoup4: `pip3 install bs4 --user`
